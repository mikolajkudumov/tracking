//
//  RestAPI.swift
//  Trackingapp
//
//  Created by Dev & Ops on 2021/9/6.
//

import Moya

let RestAPIProvider = MoyaProvider<RestAPI>()

enum RestAPI {
    case getExpenses
    case getCategories
}

extension RestAPI : TargetType {
    
    var baseURL: URL {
        
        guard let url = URL(string: BASE_URL) else { fatalError("baseURL could not be configured.")}
        return url
    }
    
    var path: String {
        
        switch self {
        case .getExpenses:
            return ENDPOINT_EXPENSES
        case .getCategories:
            return ENDPOINT_CATEGORIES
        }
    }
    var method: Moya.Method {
        switch self {
        case .getExpenses:
            return .get
        case .getCategories:
            return .get
        }
    }
    
    var sampleData: Data {
        
        return Data()
    }
    
    var task: Task {
        
        switch self {
        case .getExpenses:
            return .requestPlain
        case .getCategories:
            return .requestPlain
        }
    }
    
    var headers: [String : String]? {
        return [
            "Content-type": "application/json",
        ]
    }
}


extension Moya.Response {
    func jsonAPIResponse() throws -> Dictionary<String, Any> {
        let any = try self.mapJSON()
        guard let dic = any as? Dictionary<String, Any> else {
            throw MoyaError.jsonMapping(self)
        }
        return dic
    }
}

