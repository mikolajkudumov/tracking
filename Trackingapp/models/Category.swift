//
//  Expense.swift
//  Trackingapp
//
//  Created by Dev & Ops on 2021/9/6.
//

import Foundation

struct Category {
    var id = 0
    var title = ""
}

extension Category: Decodable {
    
    enum CategoryCodingKeys: String, CodingKey {
        case id
        case title
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CategoryCodingKeys.self)
        do {
            id = try container.decode(Int.self, forKey: .id)
        } catch let error as NSError {
            print(error)
        }
        
        do {
            title = try container.decode(String.self, forKey: .title)
        } catch let error as NSError {
            print(error)
        }
    }
}
