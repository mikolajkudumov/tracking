//
//  Expense.swift
//  Trackingapp
//
//  Created by Dev & Ops on 2021/9/6.
//

import Foundation

struct Expense {
    var id = 0
    var title = ""
    var merchant = ""
    var amount = 0
    var date = ""
    var categoryId = 0
}

extension Expense: Decodable {
    
    enum ExpenseCodingKeys: String, CodingKey {
        case id
        case title
        case merchant
        case amount
        case date
        case categoryId
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ExpenseCodingKeys.self)
        do {
            id = try container.decode(Int.self, forKey: .id)
        } catch let error as NSError {
            print(error)
        }
        
        do {
            title = try container.decode(String.self, forKey: .title)
        } catch let error as NSError {
            print(error)
        }
        
        do {
            merchant = try container.decode(String.self, forKey: .merchant)
        } catch let error as NSError {
            print(error)
        }
        
        do {
            amount = try container.decode(Int.self, forKey: .amount)
        } catch let error as NSError {
            print(error)
        }
        
        do {
            date = try container.decode(String.self, forKey: .date)
        } catch let error as NSError {
            print(error)
        }
        
        do {
            categoryId = try container.decode(Int.self, forKey: .categoryId)
        } catch let error as NSError {
            print(error)
        }
    }
}
