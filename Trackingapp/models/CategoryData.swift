//
//  Expense.swift
//  Trackingapp
//
//  Created by Dev & Ops on 2021/9/6.
//

import Foundation

struct CategoryData {
    var data = [Category]()
}

extension CategoryData: Decodable {
    
    enum CategoryDataCodingKeys: String, CodingKey {
        case data
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CategoryDataCodingKeys.self)
        do {
            data = try container.decode([Category].self, forKey: .data)
        } catch let error as NSError {
            print(error)
        }
    }
}
