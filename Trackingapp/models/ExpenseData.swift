//
//  Expense.swift
//  Trackingapp
//
//  Created by Dev & Ops on 2021/9/6.
//

import Foundation

struct ExpenseData {
    var data = [Expense]()
}

extension ExpenseData: Decodable {
    
    enum ExpenseDataCodingKeys: String, CodingKey {
        case data
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ExpenseDataCodingKeys.self)
        do {
            data = try container.decode([Expense].self, forKey: .data)
        } catch let error as NSError {
            print(error)
        }                
    }
}
