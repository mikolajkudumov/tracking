//
//  MainViewController.swift
//  Trackingapp
//
//  Created by Dev & Ops on 2021/9/6.
//

import UIKit


class MainViewController: SuperViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableview: UITableView!
    
    var dataArray: [Expense] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        getExpenses()
    }
    
    func getExpenses() {
        showHUD()
        RestAPIProvider.request(RestAPI.getExpenses, completion: { (result) in
            self.hideHUD()
            switch result {
            case .success(_):
                do {
                    let resp = try result.get()
                    let results = try resp.map(ExpenseData.self)
                    self.dataArray = results.data
                    self.tableview.reloadData()
                } catch let err {
                    print(err)
                }
            case let .failure(error):
                print(error)
            }
        })
    }
    
    // tableview datasource/delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellExpense") as! CellExpense
        cell.initData(expense: dataArray[indexPath.row])
        return cell
    }
}
