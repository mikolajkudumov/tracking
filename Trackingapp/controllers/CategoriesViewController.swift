//
//  CategoriesViewController.swift
//  Trackingapp
//
//  Created by Dev & Ops on 2021/9/6.
//

import UIKit

class CategoriesViewController: SuperViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableview: UITableView!
    var dataArray: [Category] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        getCategories()
    }
    
    @IBAction func onback(_ sender: Any) {
        self.onBack()
    }
    
    func getCategories() {
        showHUD()
        RestAPIProvider.request(RestAPI.getCategories, completion: { (result) in
            self.hideHUD()
            switch result {
            case .success(_):
                do {
                    let resp = try result.get()
                    let results = try resp.map(CategoryData.self)
                    self.dataArray = results.data
                    self.tableview.reloadData()
                } catch let err {
                    print(err)
                }
            case let .failure(error):
                print(error)
            }
        })
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellCategory") as! CellCategory
        cell.initData(category: dataArray[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
