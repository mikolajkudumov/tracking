//
//  CellExpense.swift
//  Trackingapp
//
//  Created by Dev & Ops on 2021/9/6.
//

import UIKit

class CellExpense: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func initData(expense: Expense) {
        lblTitle.text = expense.title
        lblAmount.text = "\(expense.amount)"
        lblDate.text = expense.date
    }

}
