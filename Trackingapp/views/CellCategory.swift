//
//  CellExpense.swift
//  Trackingapp
//
//  Created by Dev & Ops on 2021/9/6.
//

import UIKit

class CellCategory: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func initData(category: Category) {
        lblTitle.text = category.title
    }

}
